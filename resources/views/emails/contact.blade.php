
<p>Contact Form Received at {{ Carbon\Carbon::now()->format('d/m/Y H:i:s') }}:</p>

<ul>
    <li><strong>Full Name:</strong> {{ $form->get('full-name') }}</li>
    <li><strong>Contact Number:</strong> {{ $form->get('contact-number') }}</li>
    <li><strong>Subject:</strong> {{ $form->get('subject') }}</li>
    <li><strong>Message:</strong> {{ $form->get('message') }}</li>
</ul>

