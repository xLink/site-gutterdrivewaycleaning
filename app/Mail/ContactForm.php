<?php

namespace Cms\Mail;

use Cms\Http\Requests\ContactFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $form;

    /**
     * Create a new message instance.
     */
    public function __construct(ContactFormRequest $request)
    {
        $this->form = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')
            ->subject('Gutter & Driveway Cleaning - New Website Lead')
            ->from('info@greensenseltd.co.uk')
            ->to('info@greensenseltd.co.uk');
    }
}
