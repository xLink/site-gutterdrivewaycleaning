<?php

namespace Cms\Http\Controllers;

use Cms\Http\Requests\ContactFormRequest;
use Cms\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function postForm(ContactFormRequest $request)
    {
        Mail::send(new ContactForm($request));

        return redirect()->back()
            ->withInfo('Thanks for contacting us, we will be in touch shortly!');
    }
}
